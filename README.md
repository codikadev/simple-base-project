Estructura estándar para proyectos con menos de 4 módulos.


Dentro de la carpeta lib, Crearemos carpeta una llamada SRC en la cual pondremos las siguientes carpetas:

-	Errors:  Aquí pondremos archivos con los tipos de errores que pueden ocurrir
                           Ejemplo: exceptions. dart, failures. dart.
-	Constants:  Aquí pondremos los archivos con las constantes. Ejemplo: colors.dart
-	Utils: Aquí pondremos los archivos de las funciones que nos facilitaran el desarrollo 
                 y     evitaran la repetición de código.
-	Debug: Aquí pondremos los archivos para pruebas y simulaciones
                Ejemplo: bloc_observer.dart, fake_data.dart 
-	Themes: Aquí un archive con el o los temas de la app y todo lo concerniente al tema, 
                como configuración de este.
-	Widgets: Aquí tolos los widgets que reutilizaremos en mas de un modulo. 
                 Ejemplo:  Buttons, ProgressIndicator, AvatarBox
-	Router: Archivo con todas las rutas de la app. En caso de usar rutas con nombre o 
               Funciones generadoras de rutas.


-	Nombre del Modulo: Crearemos una carpeta con el nombre del modulo y la siguiente estructura.


-	Data 
	data_providers: Implementación del contrato del repositorio y llamadas a los proveedores de datos.
	Models: Modelos que utilizaremos en la app
	Repositories: aquí una clase abstracta que define un contrato de lo que debe hacer el data provider.

-	Logic

	Blocs: Aquí la lógica para la administración de los estados de la app que responden a las interacciones del usuario.       
Ejemplo ChangeNotifier, Cubits, FlutterBloc.

-	Presentation
	Pages: Todas las pantallas de este Modulo
	Widgets: todos los widgets que estaremos reutilizando en este modulo 


Los archivos  main, inyección de dependencias y variables de entorno, estarán fuera de estas carpetas.


[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.


**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).