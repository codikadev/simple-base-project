import 'package:flutter/material.dart';

/// Here we can add the complete pallet of collors given from the UI
/// by [adding this way] static const Color color1D85EB = Color(0xFF1D85EB);
class AppColors {
  static const Color color1D85EB = Color(0xFF1D85EB);
  static const Color color00A8E4 = Color(0xFF00A8E4);
  static const Color color0054A6 = Color(0xFF0054A6);
  static const Color color445D75 = Color(0xFF445D75);
  static const Color color9AABBC = Color(0xFF9AABBC);
  static const Color colorA4B5D7 = Color(0xFFA4B5D7);
}
