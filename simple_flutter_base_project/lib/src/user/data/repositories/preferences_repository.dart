/// Add here the functions that data providers must satisfy according to
/// the service

abstract class PreferencesRepository {
  String getUserToken();
  String getUserEmail();

  Future<void> setUserToken(String token);
  Future<void> setUserName(String name);

  DateTime getTokenExpires();
}
