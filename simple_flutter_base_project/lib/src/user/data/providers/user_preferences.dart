import 'package:shared_preferences/shared_preferences.dart';
import 'package:simple_flutter_base_project/src/user/data/repositories/preferences_repository.dart';

class UserPreferences implements PreferencesRepository {
  SharedPreferences _preferences;

  Future<SharedPreferences> get init async {
    return _preferences ??= await SharedPreferences.getInstance();
  }

  @override
  DateTime getTokenExpires() {
    // TODO: implement getTokenExpires
    throw UnimplementedError();
  }

  @override
  String getUserEmail() {
    // TODO: implement getUserEmail
    throw UnimplementedError();
  }

  @override
  String getUserToken() {
    // TODO: implement getUserToken
    throw UnimplementedError();
  }

  @override
  Future<void> setUserName(String name) {
    // TODO: implement setUserName
    throw UnimplementedError();
  }

  @override
  Future<void> setUserToken(String token) {
    // TODO: implement setUserToken
    throw UnimplementedError();
  }
}
