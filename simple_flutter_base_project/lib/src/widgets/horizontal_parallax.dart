import 'package:flutter/material.dart';
import 'package:simple_flutter_base_project/src/utils/extensions/sizes.dart';

class ParallaxRecipe extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Sizes().ctn(context);
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: SizedBox(
        height: Sizes.screenH * 0.30,
        child: Row(
          children: <Widget>[
            for (final Location location in locations)
              LocationListItem(
                imageUrl: location.imageUrl,
                name: location.name,
                country: location.place,
              ),
          ],
        ),
      ),
    );
  }
}

@immutable
class LocationListItem extends StatelessWidget {
  LocationListItem({
    Key key,
    @required this.imageUrl,
    @required this.name,
    @required this.country,
  }) : super(key: key);

  final String imageUrl;
  final String name;
  final String country;
  final GlobalKey _backgroundImageKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 24, vertical: 16),
      child: AspectRatio(
        aspectRatio: 16 / 9,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(16),
          child: Stack(
            children: <Widget>[
              _buildParallaxBackground(context),
              _buildGradient(),
              _buildTitleAndSubtitle(),
            ],
          ),
        ),
      ),
    );
  }

  // ignore: avoid_field_initializers_in_const_classes
  // final GlobalKey _backgroundImageKey = GlobalKey();
  Widget _buildParallaxBackground(BuildContext context) {
    return Flow(
      delegate: ParallaxFlowDelegate(
        //Para buscar los límites del Scrollable
        scrollable: Scrollable.of(context),
        //Para buscar los límites de su elemento de lista individual,
        listItemContext: context,
        //Para buscar el tamaño final de su imagen de fondo
        backgroundImageKey: _backgroundImageKey,
      ),
      children: <Widget>[
        Image.network(
          imageUrl,
          fit: BoxFit.cover,
        ),
      ],
    );
  }

  Widget _buildGradient() {
    return Positioned.fill(
      child: DecoratedBox(
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: <Color>[Colors.transparent, Colors.black.withOpacity(0.7)],
            begin: Alignment.topCenter,
            end: Alignment.bottomCenter,
            stops: const <double>[0.6, 0.95],
          ),
        ),
      ),
    );
  }

  Widget _buildTitleAndSubtitle() {
    return Positioned(
      left: 20,
      bottom: 20,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            name,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 20,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            country,
            style: const TextStyle(
              color: Colors.white,
              fontSize: 14,
            ),
          ),
        ],
      ),
    );
  }
}

class ParallaxFlowDelegate extends FlowDelegate {
  ParallaxFlowDelegate({
    @required this.scrollable,
    @required this.listItemContext,
    @required this.backgroundImageKey,
  }) : super(repaint: scrollable.position);
  final ScrollableState scrollable;
  final BuildContext listItemContext;
  final GlobalKey backgroundImageKey;

  // BoxConstraints getConstrainsForChild(int i, BoxConstraints constraints) {
  //   //Ajusta la imagen al ancho del padre
  //   return BoxConstraints.tightFor(width: constraints.maxWidth);
  // }

  @override
  BoxConstraints getConstraintsForChild(int i, BoxConstraints constraints) {
    //Ajusta la imagen al ancho del padre
    return BoxConstraints.tightFor(
      width: constraints.maxWidth * 1.14,
    );
  }

  @override
  void paintChildren(FlowPaintingContext context) {
    // Calculate the position of this list item within the viewport.
    final RenderBox scrollableBox =
        scrollable.context.findRenderObject() as RenderBox;
    final RenderBox listItemBox =
        listItemContext.findRenderObject() as RenderBox;
    final Offset listItemOffset = listItemBox.localToGlobal(
        listItemBox.size.center(Offset.zero),
        ancestor: scrollableBox);

    final double viewportDimension = scrollable.position.viewportDimension;
    final num scrollFraction =
        (listItemOffset.dx / viewportDimension).clamp(0.0, 1.0);
    // print(scrollFraction);
    // Calculate the horizontal alignment of the background
    // based on the scroll percentage.
    final Alignment horizontalAligment =
        Alignment((scrollFraction * -0.7 - 0.85).toDouble(), 0.0);

    // print(horizontalAligment.x);
    // Convert the background alignment into a pixel offset for
    // painting purposes.

    final Size backgroundSize = (backgroundImageKey.currentContext != null)
        ? (backgroundImageKey.currentContext.findRenderObject() as RenderBox)
            .size
        : Size.zero;

    final Size listItemSize = context.size;
    final Rect childRect =
        horizontalAligment.inscribe(backgroundSize, Offset.zero & listItemSize);

    // Paint the background.
    context.paintChild(
      0,
      transform: Transform.translate(
        offset: Offset(
          childRect.left,
          0.0,
        ),
      ).transform,
    );
  }

  @override
  bool shouldRepaint(ParallaxFlowDelegate oldDelegate) {
    // TODO: implement shouldRepaint
    // return true;
    return scrollable != oldDelegate.scrollable ||
        listItemContext != oldDelegate.listItemContext ||
        backgroundImageKey != oldDelegate.backgroundImageKey;
  }
}

class Location {
  const Location({
    @required this.name,
    @required this.place,
    @required this.imageUrl,
  });

  final String name;
  final String place;
  final String imageUrl;
}

const String urlPrefix =
    'https://flutter.dev/docs/cookbook/img-files/effects/parallax';
const List<Location> locations = <Location>[
  Location(
    name: 'Mount Rushmore',
    place: 'U.S.A',
    imageUrl: '$urlPrefix/01-mount-rushmore.jpg',
  ),
  Location(
    name: 'Singapore',
    place: 'China',
    imageUrl: '$urlPrefix/02-singapore.jpg',
  ),
  Location(
    name: 'Machu Picchu',
    place: 'Peru',
    imageUrl: '$urlPrefix/03-machu-picchu.jpg',
  ),
  Location(
    name: 'Vitznau',
    place: 'Switzerland',
    imageUrl: '$urlPrefix/04-vitznau.jpg',
  ),
  Location(
    name: 'Bali',
    place: 'Indonesia',
    imageUrl: '$urlPrefix/05-bali.jpg',
  ),
  Location(
    name: 'Mexico City',
    place: 'Mexico',
    imageUrl: '$urlPrefix/06-mexico-city.jpg',
  ),
  Location(
    name: 'Cairo',
    place: 'Egypt',
    imageUrl: '$urlPrefix/07-cairo.jpg',
  ),
];
