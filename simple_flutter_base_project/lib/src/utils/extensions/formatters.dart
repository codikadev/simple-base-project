extension Formatters on String {
  ///Capitalize first letter of the first word in the string
  String capitalize() => this == null
      ? null
      : "${this[0].toUpperCase()}${substring(1).toLowerCase()}";

  ///Capitalize first letter of each word
  String capitalizeAll() => this == null
      ? this
      : split(' ').map((String e) {
          String aux = e;
          try {
            aux = "${e[0].toUpperCase()}${e.substring(1).toLowerCase()}";
          } catch (e) {}
          return aux;
        }).join(' ');

  ///Phone number must be a plain [String]
  String phoneNomberFormat() {
    final String tele = this;
    List<String> telefono1;
    String formattedPhoneNumber = "";
    final String t = tele;
    if (t != '') {
      if (t.contains(";")) {
        telefono1 = t.split(";");
        final String nf = telefono1[0]
            .replaceAll("-", "")
            .replaceAll(" ", "")
            .replaceAll("(", "")
            .replaceAll(")", "");
        formattedPhoneNumber =
            "(${nf.substring(0, 3)}) ${nf.substring(3, 6)}-${nf.substring(6, nf.length)}";
      } else {
        final String nf = t
            .replaceAll("-", "")
            .replaceAll(" ", "")
            .replaceAll("(", "")
            .replaceAll(")", "");
        formattedPhoneNumber =
            "(${nf.substring(0, 3)}) ${nf.substring(3, 6)}-${nf.substring(6, nf.length)}";
      }
    }

    return formattedPhoneNumber;
  }

  ///Phone number must be a plain [String]
  String phoneSMSClean() {
    final String tele = this;
    List<String> telefono1;
    String formattedPhoneNumber = "";
    final String t = tele;
    if (t != '') {
      if (t.contains(";")) {
        telefono1 = t.split(";");
        final String nf = telefono1[0]
            .replaceAll("-", "")
            .replaceAll(" ", "")
            .replaceAll("(", "")
            .replaceAll(")", "");
        formattedPhoneNumber = nf;
      } else {
        final String nf = t
            .replaceAll("-", "")
            .replaceAll(" ", "")
            .replaceAll("(", "")
            .replaceAll(")", "");
        formattedPhoneNumber = nf;
      }
    }

    return formattedPhoneNumber;
  }

  ///Phone number must be a plain [String]
  String phoneMask() {
    final String tele = this;
    final String formattedPhoneNumber =
        "*** *** ${tele.substring(6, tele.length)}";

    return formattedPhoneNumber;
  }

  ///Card number must be a plain [String]
  String cardMask() {
    final String cardN = this;
    final String formattedPhoneNumber =
        "${cardN.substring(0, 4)} **** **** ${cardN.substring(12, cardN.length)}";

    return formattedPhoneNumber;
  }
}
