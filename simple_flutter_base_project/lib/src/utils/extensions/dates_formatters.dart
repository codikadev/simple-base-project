import 'package:intl/intl.dart';
import 'formatters.dart';

class DatesFormatters {
  /// Given a String [date] with a N1/N2/N3
  /// Returns a new [dateFormat] --> N3/N2/N1
  String dateConvertFilter(String date) {
    final List<String> sp = date.split("/");
    final String dateFormat =
        "${sp[2].toString()}-${sp[1].toString()}-${sp[0].toString()}";

    return dateFormat;
  }

  /// Given a String [date] with a N1/N2/N3
  /// Returns a new [dateFormat] --> N3-N2-N1
  String fromDDMMYYtoYYMMDD(String date) {
    final List<String> sp = date.split("/");
    final String day = sp[0].padLeft(2, '0');
    final String month = sp[1].padLeft(2, '0');

    final String dateFormat = "${sp[2].toString()}/$day/$month";

    return dateFormat;
  }
}

extension DatesFormattersExt on String {
  /// Given a String [date] with a N1/N2/N3
  /// Returns a new [dateFormat] --> N3-N2-N1
  String dateFilter() {
    final List<String> sp = split("/");
    return "${sp[2].toString()}-${sp[1].toString()}-${sp[0].toString()}";
  }

  /// Given a String [date] with a N1/N2/N3
  /// Returns a new [dateFormat] --> N3/N2/N1
  String dateFilter2() {
    final List<String> sp = split("/");
    return "${sp[2].toString()}/${sp[1].toString()}/${sp[0].toString()}";
  }

  String convetirFechaDMMYY() {
    final List<String> sp = split("/");

    final String diaaa = sp[0].padLeft(2, '0');

    return "${sp[2].toString()}/$diaaa/${sp[1].toString()}";
  }

  DateTime dmyToDate({String separator = '/'}) {
    final List<String> strList = this?.split(separator);
    if (strList == null || strList.length != 3) return null;
    return DateTime(
        int.parse(strList[2]), int.parse(strList[1]), int.parse(strList[0]));
  }
}

extension DateTimeExtension on DateTime {
  //?!Preguntar a Samuel
  // String dmy({String separator = '-'}) => this == null
  //     ? this
  //     : DateFormat('dd${separator}MM${separator}yyyy').format(this);

  String dMMMMy({String separator = '-'}) =>
      DateFormat('d MMMM y', 'es').format(this).capitalizeAll();

  DateTime endOfPreviousMonth() => DateTime(year, month, 0);

  // ignore: avoid_redundant_argument_values
  DateTime startOfPreviousMonth() => DateTime(year, month - 1, 1);

  DateTime startOfPreviousThreeMonth() => DateTime(year, month - 3);

  DateTime startOfPreviousSixMonth() => DateTime(year, month - 6);

  String yMd() => DateFormat.yMd().format(this);

  //Format to month's name and year. e.g ENERO 2021
  String monthNameYear() =>
      DateFormat("MMMM yyy", 'ES').format(this).toUpperCase();

  String dayMonthYear() =>
      DateFormat("dd MMMM yyy", 'ES').format(this).capitalizeAll();

  String get hora => DateFormat.jm().format(this);

  String monthName() => DateFormat("MMMM", 'ES').format(this).toUpperCase();
}
