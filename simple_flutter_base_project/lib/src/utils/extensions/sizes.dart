import 'package:flutter/material.dart';

class Sizes {
  static double _width;
  static double _height;

  /// Takes the current [Context] to get maxWidth and maxHeight of device screen
  void ctn(BuildContext context) {
    _width = MediaQuery.of(context).size.width;
    _height = MediaQuery.of(context).size.height;
  }

  static double height(double i) {
    return _height * (i / 812);
  }

  static double width(double i) {
    return _width * i / 375;
  }

  /// Takes the current [Context] to get maxWidth of device screen
  static double get screenW => _width;

  /// Takes the current [Context] to get maxHeigth of device screen
  static double get screenH => _height;
}

extension SizesExt on double {
  //How to use

  /// You must call the method [ Sizes().ctn(context);] on build
  ///
  ///
  ///Whenever you use height then use this method like this => [20.0.h]
  ///
  double get h => Sizes.height(this);

  /// You must call the method [ Sizes().ctn(context);] on build
  ///
  ///
  ///Whenever you use width then use this method like this => [10.0.w]
  ///
  double get w => Sizes.width(this);
}

TextStyle textos(
    {BuildContext ctn,
    double fSize = 16,
    FontWeight fontWeight = FontWeight.normal,
    Color color,
    String fontFamily = 'Inter'}) {
  final double itemSize = fSize / 375;
  return TextStyle(
    fontSize: MediaQuery.of(ctn).size.width * itemSize,
    fontWeight: fontWeight,
    color: color,
    fontFamily: fontFamily,
    decoration: TextDecoration.none,
  );
}

/// [tipo] 1 si es para el width y 2 si es para el height .
/// [fSize] tamaño deseado .
double widthHeight({BuildContext ctn, double fSize, int tipo}) {
  final double itemSize = fSize / ((tipo == 1) ? 375 : 812);
  final double t = tipo == 1
      ? MediaQuery.of(ctn).size.width
      : MediaQuery.of(ctn).size.height;
  return t * itemSize;
}
