import 'dart:async';

class Validators {
  // ignore: always_specify_types
  final validarEmail =
      StreamTransformer<String, String>.fromHandlers(handleData: (
    String email,
    EventSink<String> sink,
  ) {
    const Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    // ignore: argument_type_not_assignable

    final RegExp regExp = RegExp(pattern);
    if (regExp.hasMatch(email)) {
      sink.add(email);
    } else {
      sink.addError('Correo invalido');
    }
  });

  ///
  StreamTransformer<String, String> get validarPassword =>
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String password, EventSink<String> sink) {
        if (password.length >= 6) {
          return sink.add(password);
        } else {
          return sink.addError('Más de 6 caracteres por favor');
        }
      });

  StreamTransformer<String, String> get validarCedula =>
      StreamTransformer<String, String>.fromHandlers(
          handleData: (String password, EventSink<String> sink) {
        if (password.length >= 13) {
          sink.add(password);
        } else {
          sink.addError('Más de 13 caracteres por favor');
        }
      });
}
