# simple_flutter_base_project

Estructura estándar para proyectos con menos de 4 módulos.


Dentro de la carpeta lib, Crearemos carpeta una llamada SRC en la cual pondremos las siguientes carpetas:

-	Errors:  Aquí pondremos archivos con los tipos de errores que pueden ocurrir
                           Ejemplo: exceptions. dart, failures. dart.
-	Constants:  Aquí pondremos los archivos con las constantes. Ejemplo: colors.dart
-	Utils: Aquí pondremos los archivos de las funciones que nos facilitaran el desarrollo 
                 y     evitaran la repetición de código.
-	Debug: Aquí pondremos los archivos para pruebas y simulaciones
                Ejemplo: bloc_observer.dart, fake_data.dart 
-	Themes: Aquí un archive con el o los temas de la app y todo lo concerniente al tema, 
                como configuración de este.
-	Widgets: Aquí tolos los widgets que reutilizaremos en mas de un modulo. 
                 Ejemplo:  Buttons, ProgressIndicator, AvatarBox
-	Router: Archivo con todas las rutas de la app. En caso de usar rutas con nombre o 
               Funciones generadoras de rutas.


-	Nombre del Modulo: Crearemos una carpeta con el nombre del modulo y la siguiente estructura.


-	Data 
	data_providers: Implementación del contrato del repositorio y llamadas a los proveedores de datos.
	Models: Modelos que utilizaremos en la app
	Repositories: aquí una clase abstracta que define un contrato de lo que debe hacer el data provider.

-	Logic

	Blocs: Aquí la lógica para la administración de los estados de la app que responden a las interacciones del usuario.       
Ejemplo ChangeNotifier, Cubits, FlutterBloc.

-	Presentation
	Pages: Todas las pantallas de este Modulo
	Widgets: todos los widgets que estaremos reutilizando en este modulo 


Los archivos  main, inyección de dependencias y variables de entorno, estarán fuera de estas carpetas.


[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
